@ECHO OFF
docker run -v %~dp0/:/repo -it --rm gittools/gitversion /repo /showvariable SemVer>version.txt
call fixversionfile.cmd
SET /p version=<%~dp0\version.fix.txt
SET repository=akitlin/sqlcmd-git-automation
SET tag=%repository%:%version%
ECHO %version%
ECHO %repository%
ECHO %tag%
docker build -t %tag% .
docker push %tag%