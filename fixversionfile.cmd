@echo off
setlocal EnableExtensions EnableDelayedExpansion
set "INTEXTFILE=version.txt"
set "OUTTEXTFILE=version.fix.txt"

for /F "tokens=2,* delims== " %%a in ('type "%INTEXTFILE%"') do (
   echo %%a>%OUTTEXTFILE%
)
endlocal